All settings for SublimeText 3

## Package Installed
https://packagecontrol.io/

* PackageControle
* AdvancedNewFile
* AllAutocomplete
* CloseOtherWindows
* DashDoc
* DocBlockr
* Git
* GitGutter
* RailsStatusSnippets
* SideBarEnhancements
* SublimeLinter-rubocop
* SublimeLinter-ruby
* TrailingSpaces
* Material Theme
* Material Theme Appbar
* A File Icon


## User preferences
*SublimeText -> Preferences -> Settings

```
{
	"auto_complete": true,
	"auto_complete_commit_on_tab": true,
	"color_scheme": "Packages/Material Theme/schemes/Material-Theme-Darker.tmTheme",
	"copy_with_empty_selection": true,
	"ensure_newline_at_eof_on_save": true,
	"font_size": 12,
	"ignored_packages":
	[
		"Vintage"
	],
	"index_files": true,
	"rulers":
	[
		79
	],
	"show_encoding": true,
	"tab_size": 2,
	"translate_tabs_to_spaces": true,
	"trim_trailing_white_space_on_save": true,
	"show_errors_on_save": true,
        "theme": "Material-Theme.sublime-theme",
        //material theme options
        "material_theme_tree_headings": true ,
        "always_show_minimap_viewport" : true,
        "bold_folder_labels"           : true,
        "font_options"                 : [ "gray_antialias", "subpixel_antialias" ],    // On retina Mac & Windows
        "indent_guide_options"         : [ "draw_normal", "draw_active" ],   // Highlight active indent
        "line_padding_bottom"          : 3,
        "line_padding_top"             : 3,
        "overlay_scroll_bars"          : "enabled",
        "material_theme_big_fileicons"            : true , // Show bigger file type icons
        "material_theme_bullet_tree_indicator"    : true , // Set a bullet as active tree indicator

}
```

## KeyBinding
*SublimeText -> Preferences -> Key Binding

```
[
  { "keys": ["super+\\"], "command": "toggle_side_bar" },
  { "keys": ["super+shift+\\"], "command": "reveal_in_side_bar"},
  { "keys": ["super+shift+w"], "command": "close_other_tabs" },

  { "keys": ["super+backspace"], "command": "run_macro_file", "args": {"file": "res://Packages/Default/Delete Line.sublime-macro"} },

  { "keys": ["super+shift+r"], "command": "goto_symbol_in_project" },
  { "keys": ["super+alt+r"], "command": "goto_definition" },

  { "keys": ["super+shift+t"], "command": "reopen_last_file" },
]
```

## Ruby Settings
*To get to the Ruby settings, open a Ruby file in the editor and then navigate the menus to
Sublime Text → Preferences → Settings – More → Syntax Specific – User.
```
{
  "word_separators": "./\\()\"'-:,.;<>~@#$%^&*|+=[]{}`~"
}
```

## Linter Settings

Find rubocop path :

hash -r
which rubocop

```
"paths": {
  "linux": [],
  "osx": [
    "/Users/invoxis/.rvm/gems/ruby-2.3.1/bin/",
    "/Users/invoxis/.rvm/bin"
  ],
  "windows": []
},
```

##Snippets
<snippet>
  <content><![CDATA[puts "\e[${1:color}m  ${2:input}===>#{${2:input}}=================================\e[0m"]]></content>
  <tabTrigger>pcol</tabTrigger>
  <description>Puts colored</description>
</snippet>


## Tips
https://mattbrictson.com/sublime-text-3-recommendations
http://docs.sublimetext.info/en/latest/index.html